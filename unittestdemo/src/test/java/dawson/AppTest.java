package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    @Test
    public void shouldReturn5WhenEqual5(){
        App application = new App();
        assertEquals("Testing if echo works", 5, application.echo(5));
    }
    @Test
    public void oneMoreShouldBeOneMore(){
        App apps = new App();
        assertEquals("Testing if oneMore works", 6, apps.oneMore(5));
    }
}
